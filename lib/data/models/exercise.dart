class Exercise {
  final int index;
  final bool finished;
  final String route;
  final double x;
  final double y;

  const Exercise(
      {required this.index, required this.finished, required this.route, required this.x, required this.y});

  Exercise copyWith({bool? finished, double? x, double? y}) => Exercise(
      index: index, finished: finished ?? this.finished, route: route, x: x ?? this.x, y: y ?? this.y);
}
