import 'package:flutter/material.dart';
import 'package:tablet_license/ui/layout.dart';
import 'package:tablet_license/ui/widgets/drag/drag_target_widget.dart';
import 'package:tablet_license/ui/widgets/drag/draggable_text_widget.dart';

class NamingExerciseScreen extends StatefulWidget {
  static String route = 'exercises/naming';

  const NamingExerciseScreen({super.key});

  @override
  State<NamingExerciseScreen> createState() => _NamingExerciseScreenState();
}

class _NamingExerciseScreenState extends State<NamingExerciseScreen> {
  final List<Item> _items = <Item>[
    Item('Frontkamera', false),
    Item('Display', false),
    Item('App-Symbol', false),
    Item('Lautstärketasten', false),
    Item('Home-Taste', false),
    Item('Lautsprecher', false),
    Item('An/ Aus Knopf & Standbytaste', false),
    Item('Anschluss für das Ladekabel', false),
    Item('Anschluss für den Kopfhörer', false),
  ];

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: const Text('Das Tablet kennenlernen')),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(SPACE_DEFAULT),
              child: Stack(
                children: [
                  const Center(child: FlutterLogo(size: 250)),
                  Align(alignment: Alignment.topCenter, child: _buildTarget(0))
                  // TODO: add remaining drag targets
                ],
              ),
            ),
            const SizedBox(height: SPACE_DEFAULT),
            Wrap(
                children: _items
                    .where((Item item) => !item.solved)
                    .map((Item item) => DraggableTextWidget(item: item.label))
                    .toList(growable: false))
          ],
        ),
      );

  Widget _buildTarget(int i) => _items[i].solved
      ? Card(
          child: Padding(
              padding: const EdgeInsets.all(SPACE_DEFAULT),
              child: Text(_items[i].label)))
      : DragTargetWidget(
          item: _items[i].label,
          onAccepted: () => setState(() {
            _items[i] = Item(_items[i].label, true);
          }),
        );
}

class Item {
  final String label;
  final bool solved;

  Item(this.label, this.solved);
}
