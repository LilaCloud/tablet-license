import 'package:flutter/material.dart';
import 'package:tablet_license/data/models/exercise.dart';
import 'package:tablet_license/ui/layout.dart';

class ExerciseTileWidget extends StatelessWidget {
  final Exercise exercise;

  const ExerciseTileWidget({super.key, required this.exercise});

  @override
  Widget build(BuildContext context) =>
      Positioned(left: exercise.x, top: exercise.y, child: InkWell(
        onTap: () => Navigator.of(context).pushNamed(exercise.route),
        child: Card(child: Padding(
          padding: const EdgeInsets.all(SPACE_DEFAULT),
          child: Text((exercise.index + 1).toString()),
        )),
      ));
}
