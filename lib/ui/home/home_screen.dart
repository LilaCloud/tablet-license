import 'package:flutter/material.dart';
import 'package:tablet_license/data/models/exercise.dart';
import 'package:tablet_license/ui/home/exercise_tile_widget.dart';
import 'package:tablet_license/ui/naming/naming_exercise_sceen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: const Text('Tablet Führerschein')),
        body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            final List<Exercise> exercises = _buildExercises(context, constraints);
            return Stack(
              children:
                  exercises.map((Exercise exercise) => ExerciseTileWidget(exercise: exercise)).toList(growable: false),
            );
          },
        ),
      );

  List<Exercise> _buildExercises(BuildContext context, BoxConstraints constraints) => <Exercise>[
    Exercise(
        index: 0,
        finished: false,
        route: NamingExerciseScreen.route,
        x: constraints.maxWidth / 10,
        y: constraints.maxHeight / 10),
    Exercise(
        index: 1, finished: false, route: '', x: constraints.maxWidth / 8, y: constraints.maxHeight / 8),
    Exercise(
        index: 2,
        finished: false,
        route: '',
        x: constraints.maxWidth - constraints.maxWidth / 8,
        y: constraints.maxHeight / 11)
  ];
}
