import 'package:flutter/material.dart';
import 'package:tablet_license/ui/layout.dart';

class DraggableTextWidget extends StatelessWidget {
  final String item;

  const DraggableTextWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) => LongPressDraggable<String>(
        data: item,
        dragAnchorStrategy: pointerDragAnchorStrategy,
        feedback: Material(key: ValueKey(item),child: _buildCard()),
        childWhenDragging: Opacity(
          opacity: 0.50,
          child: _buildCard()
        ),
        child: _buildCard(),
      );

  Widget _buildCard() => Card(
      child: Padding(
          padding: const EdgeInsets.all(SPACE_DEFAULT), child: Text(item)));
}
