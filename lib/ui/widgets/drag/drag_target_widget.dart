import 'package:flutter/material.dart';
import 'package:tablet_license/ui/layout.dart';

class DragTargetWidget extends StatelessWidget {

  final String item;
  final VoidCallback onAccepted;

  const DragTargetWidget({super.key, required this.item, required this.onAccepted});

  @override
  Widget build(BuildContext context) =>
      DragTarget<String>(
        builder: (context, candidateItems, rejectedItems) {
          return Container(width: 100,
              height: 50,
              decoration: BoxDecoration(border: Border.all(color: COLOR_BORDER),
                  borderRadius: BorderRadius.circular(RADIUS_DEFAULT)));
        },
        onAccept: (item) {
          if (item == this.item) {
            onAccepted();
          }
        },
      );

}