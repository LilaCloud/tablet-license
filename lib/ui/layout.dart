import 'package:flutter/material.dart';

const double SPACE_XX_LARGE = 36.0;
const double SPACE_X_LARGE = 24.0;
const double SPACE_LARGE = 18.0;
const double SPACE_DEFAULT = 12.0;
const double SPACE_DRAGGABLE = 12.0;
const double SPACE_SMALL = 6.0;
const double SPACE_TINY = 3.0;

const double SIZE_TRACKER_BOX = 18.0;
const double SIZE_CHARACTER_LIST_ITEM_IMAGE = 50.0;
const double SIZE_DIALOG = 0.7;

const double SPACE_CHECKBOX_ICON = 11.0;
const double RADIUS_DEFAULT = 2.0;

const double MAX_WIDTH_CONTENT = 670.0;
const double MAX_WIDTH_CONTENT_THRESHOLD = 700.0;
const double BIG_SCREEN_THRESHOLD = 900.0;

const double OPACITY_DRAGGING = 0.2;
const double OPACITY_DISABLED = 0.6;
const double OPACITY_FULL = 1.0;
const double OPACITY_BACKGROUND = 0.05;

const Color COLOR_RED_DARK = Color(0xff770c0f);
const Color COLOR_RED = Color(0xffC4171D);
const Color COLOR_RED_TRANSLUCENT = Color(0x66C4171D);
const Color COLOR_BORDER = Colors.grey;
