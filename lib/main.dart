import 'package:flutter/material.dart';
import 'package:tablet_license/ui/home/home_screen.dart';
import 'package:tablet_license/ui/naming/naming_exercise_sceen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tablet Führerschein',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomeScreen(),
      routes: <String, WidgetBuilder>{
        NamingExerciseScreen.route : (BuildContext context) => const NamingExerciseScreen(),
      },
    );
  }
}